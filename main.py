#!/usr/bin/env python
import random

same = {
        'o' : 'о',
        'e' : 'е',
        'E' : 'Е',
        'T' : 'Т',
        'y' : 'у',
        'Y' : 'У',
        'p' : 'р',
        'P' : 'Р',
        'a' : 'а',
        'A' : 'А',
        'H' : 'Н',
        'K' : 'К',
        'x' : 'х',
        'X' : 'Х',
        'c' : 'с',
        'C' : 'С',
        'B' : 'В',
        'M' : 'М',
        'O' : '0',
        'О' : '0'
}


def main(text):
    res = ""
    
    for ch in text:
        if bool(random.getrandbits(1)):
            if ch in same.keys():
                res += same.get(ch)
            elif ch in same.values():
                for k, v in same.items():
                    if v == ch:
                        res += k
                        break
            else:
                res += ch
        else:
            res += ch
    
    return res


if __name__ == "__main__":
    import sys
    if len(sys.argv) < 2:
        _text = None
        text = ""
        while _text != "":
            _text = input()
            if _text != "":
                text += _text + "\n"
        print(main(text))
        exit(0)
    print(main(" ".join(sys.argv[1:])))
    exit(0)
